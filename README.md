## SIMPLE GAME OF LIFE

A simple implementation of [Conway's game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) for Android Apps. It is currently published on the Android Play store via [this link](https://play.google.com/store/apps/details?id=com.technica.idea.gameoflife).
