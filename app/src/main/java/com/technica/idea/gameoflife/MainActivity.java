package com.technica.idea.gameoflife;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    ConstraintLayout container;

    GridLayout switchBoard;
    private int boardWidth;
    private int boardHeight;
    private int[][] boardState;
    private int containerWidth;

    private Handler handler;
    private Runnable runnable;

    private TextView block;

    private  int timerMillis;

    Button stepButton, runButton, clearButton, aboutButton, settingsButton;

    private boolean running;

    boolean deadButton = false;

    private int aliveCellColour, deadCellColour;

    Switch wrapSwitch;

    boolean wrapAround;

    private void wrapGrid() {
        int rows = boardState.length;
        int cols = boardState[0].length;

        for (int j = 1; j < cols - 1; j++) {
            boardState[0][j] = wrapAround ? boardState[rows - 2][j] : 0;
        }

        for (int j = 1; j < cols - 1; j++) {
            boardState[rows - 1][j] = wrapAround ? boardState[1][j] : 0;
        }

        for (int i = 1; i < rows - 1; i++) {
            boardState[i][0] = wrapAround ? boardState[i][cols - 2] : 0;
        }

        for (int i = 1; i < rows - 1; i++) {
            boardState[i][cols - 1] = wrapAround ? boardState[i][1] : 0;
        }

        boardState[0][0] = wrapAround ? boardState[rows - 2][cols - 2] : 0;
        boardState[rows - 1][cols - 1] = wrapAround ? boardState[1][1] : 0;
        boardState[0][cols - 1] = wrapAround ? boardState[rows - 2][1] : 0;
        boardState[rows - 1][0] = wrapAround ? boardState[1][cols - 2] : 0;
    }

    public void colorButton(View v) {
        final Button b = (Button) v;
        deadButton = b.getId() != R.id.aliveButton;

        settingsButton.animate().alpha(0.4f).setDuration(200);

        GridLayout colourGrid = findViewById(R.id.colourGrid);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        assert layoutInflater != null;
        View layout = layoutInflater.inflate(R.layout.colour_popup_layout, colourGrid);

        GridLayout cGrid = layout.findViewById(R.id.colourGrid);

        final PopupWindow colorPopup = new PopupWindow(this);

        int cellWidth = (int) containerWidth / 10;

        int cellPadding = 2;

        for (int i = 0; i < 25; i++) {
            Button button = new Button(getApplicationContext());

            GridLayout.LayoutParams lp = new GridLayout.LayoutParams();

            lp.width = cellWidth;
            lp.height = cellWidth;
            lp.setMargins(cellPadding, cellPadding, cellPadding, cellPadding);

            String colourName = "color" + Integer.toString(i+1);

            final int colour = getResources().getIdentifier(colourName, "color", getPackageName());

            button.setBackgroundColor(getResources().getColor(colour));


            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    colorPopup.dismiss();
                    b.setBackgroundColor(getResources().getColor(colour));

                    if (deadButton) {
                        deadCellColour = getResources().getColor(colour);
                    } else {
                        aliveCellColour = getResources().getColor(colour);
                    }
                }
            });

            button.setLayoutParams(lp);

//            button.setBackground(getResources().getDrawable(R.drawable.button_border));

            cGrid.addView(button);
        }

        colorPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                settingsButton.animate().alpha(1f).setDuration(200);
            }
        });

        colorPopup.setContentView(layout);

        colorPopup.setWidth(ListPopupWindow.WRAP_CONTENT);
        colorPopup.setHeight(ListPopupWindow.WRAP_CONTENT);

        colorPopup.setFocusable(true);

        colorPopup.setAnimationStyle(R.style.about_animation);

        colorPopup.setBackgroundDrawable(new BitmapDrawable());

        colorPopup.showAtLocation(layout, Gravity.CENTER, 0, -50);

    }

    public void play(View v) {
        if (v.getTag().toString().equals("0")) {
            running = true;
            v.setBackground(getResources().getDrawable(R.drawable.pause));
            v.setTag("1");
            stepFunction();
            handler.postDelayed(runnable, timerMillis);
        } else {
            running = false;
            v.setBackground(getResources().getDrawable(R.drawable.play));
            v.setTag("0");
            handler.removeCallbacks(runnable);
        }

    }

    private void runTimer(int millis) {

        timerMillis = millis;

        final int m = millis;

        runnable = new Runnable() {
            @Override
            public void run() {
                stepFunction();
                handler.postDelayed(this, m);
            }
        };
    }

    public void about(View v) {
        if (running)
            handler.removeCallbacks(runnable);

        block.setVisibility(View.VISIBLE);
        block.animate().alpha(0.8f).setDuration(200);
        stepButton.animate().alpha(0.2f).setDuration(200);
        runButton.animate().alpha(0.2f).setDuration(200);
        clearButton.animate().alpha(0.2f).setDuration(200);
        settingsButton.animate().alpha(0.2f).setDuration(200);
        aboutButton.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        assert layoutInflater != null;
        View layout = layoutInflater.inflate(R.layout.about_popup_layout, linearLayout);

        final PopupWindow aboutPopup = new PopupWindow(this);

        aboutPopup.setContentView(layout);

        int popupWindowWidth = (int) (4* containerWidth /5);

        aboutPopup.setWidth(popupWindowWidth);
        aboutPopup.setHeight(ListPopupWindow.WRAP_CONTENT);
        aboutPopup.setFocusable(true);

        aboutPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                block.animate().alpha(0f).setDuration(200);
                stepButton.animate().alpha(1f).setDuration(200);
                runButton.animate().alpha(1f).setDuration(200);
                clearButton.animate().alpha(1f).setDuration(200);
                settingsButton.animate().alpha(1f).setDuration(200);
                aboutButton.animate().scaleX(1f).scaleY(1f).setDuration(200);

                if (running) {
                    stepFunction();
                    runTimer(timerMillis);
                    handler.postDelayed(runnable, timerMillis);
                }

            }
        });

        aboutPopup.setAnimationStyle(R.style.about_animation);

        aboutPopup.setBackgroundDrawable(new BitmapDrawable());

        aboutPopup.showAtLocation(layout, Gravity.CENTER, 0, -50);

        Button dismissPopup = layout.findViewById(R.id.button_dismiss);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aboutPopup.dismiss();
            }
        });
    }

    public void settings(View v) {
        if (running)
            handler.removeCallbacks(runnable);

        block.setVisibility(View.VISIBLE);
        block.animate().alpha(0.8f).setDuration(200);
        stepButton.animate().alpha(0.2f).setDuration(200);
        runButton.animate().alpha(0.2f).setDuration(200);
        clearButton.animate().alpha(0.2f).setDuration(200);
        aboutButton.animate().alpha(0.2f).setDuration(200);
        settingsButton.animate().scaleX(1.2f).scaleY(1.2f).setDuration(200);


        LinearLayout settingsLayout = (LinearLayout) findViewById(R.id.settingsPopup);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = layoutInflater.inflate(R.layout.settings_popup_layout, settingsLayout);

        final PopupWindow settingsPopup = new PopupWindow(this);

        settingsPopup.setContentView(layout);

        int popupWindowWidth = (int) (4* containerWidth /5);

        settingsPopup.setWidth(popupWindowWidth);
        settingsPopup.setHeight(ListPopupWindow.WRAP_CONTENT);
        settingsPopup.setFocusable(true);

        final SeekBar timeSeek = layout.findViewById(R.id.timeSeek);

        final int minTime = 50;

        timeSeek.setProgress(timerMillis-minTime);

        timeSeek.setMax(2000 - minTime);

        final TextView timeView = layout.findViewById(R.id.millisView);
        timeView.setText(Integer.toString(timerMillis) + " ms");

        timeSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                timerMillis = timeSeek.getProgress() + minTime;
                timeView.setText(Integer.toString(timerMillis) + " ms");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        settingsPopup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                block.animate().alpha(0f).setDuration(200);
                stepButton.animate().alpha(1f).setDuration(200);
                runButton.animate().alpha(1f).setDuration(200);
                clearButton.animate().alpha(1f).setDuration(200);
                aboutButton.animate().alpha(1f).setDuration(200);
                settingsButton.animate().scaleX(1f).scaleY(1f).setDuration(200);

                runTimer(timerMillis);

                if (running) {
                    stepFunction();

                    handler.postDelayed(runnable, timerMillis);
                }

                for (int i = 0; i < switchBoard.getChildCount(); i++) {
                    Button b = (Button) switchBoard.getChildAt(i);
                    try{
                        int buttonTag = Integer.parseInt(b.getTag().toString());
                        int row = (int) buttonTag / 10;
                        int col = buttonTag % 10;

                        if (boardState[row + 1][col + 1] == 0) {
                            b.setBackgroundColor(deadCellColour);
                        } else {
                            b.setBackgroundColor(aliveCellColour);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        settingsPopup.setAnimationStyle(R.style.about_animation);

        settingsPopup.setBackgroundDrawable(new BitmapDrawable());

        settingsPopup.showAtLocation(layout, Gravity.CENTER, 0, -50);

        Button dismissPopup = layout.findViewById(R.id.button_dismiss);
        dismissPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsPopup.dismiss();
            }
        });

        Button aliveButton = layout.findViewById(R.id.aliveButton);
        aliveButton.setBackgroundColor(aliveCellColour);

        Button deadButton = layout.findViewById(R.id.deadButton);
        deadButton.setBackgroundColor(deadCellColour);

        wrapSwitch = layout.findViewById(R.id.wrapSwitch);
        wrapSwitch.setChecked(wrapAround);

        wrapSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wrapAround = isChecked;
            }
        });

    }

    public void clear(View v) {
        for (int i = 0; i < switchBoard.getChildCount(); i++) {
            Button b = (Button) switchBoard.getChildAt(i);
            try{
                int buttonTag = Integer.parseInt(b.getTag().toString());
                int row = (int) buttonTag / 10;
                int col = buttonTag % 10;

                if (boardState[row + 1][col + 1] == 1) {
                    cellState(b);
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void stepFunction() {
        wrapGrid();

        int rowSize = boardState.length;
        int colSize = boardState[0].length;

        int tempState[][] = new int[rowSize][colSize];

        for (int[] r : tempState) {
            Arrays.fill(r, 0);
        }

        for (int i = 1; i < rowSize-1; i++) {
            for (int j = 1; j < colSize-1; j++) {
                // Count the neighbors of each cell
                int neighborCount = 0;

                try {
                    // Neighbors
                    int topLeft = boardState[i-1][j-1];
                    int topMid = boardState[i-1][j];
                    int topRight = boardState[i-1][j+1];
                    int midLeft = boardState[i][j-1];
                    int midRight = boardState[i][j+1];
                    int bottomLeft = boardState[i+1][j-1];
                    int bottomMid = boardState[i+1][j];
                    int bottomRight = boardState[i+1][j+1];

                    int[] n = {topLeft, topMid, topRight, midLeft, midRight, bottomLeft, bottomMid, bottomRight};

                    for (int p: n) {
                        neighborCount += p;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // If alive neighbors are less than 2 or greater than 3 kill, neighbors are 3, live
                if (neighborCount < 2 || neighborCount > 3) {
                    tempState[i][j] = 0;
                } else if (neighborCount == 2 && boardState[i][j] == 0) {
                    tempState[i][j] = 0;
                } else {
                    tempState[i][j] = 1;
                }


            }
        }

        for (int i = 0; i < switchBoard.getChildCount(); i++) {
            Button b = (Button) switchBoard.getChildAt(i);
            try{
                int buttonTag = Integer.parseInt(b.getTag().toString());
                int row = (int) buttonTag / 10;
                int col = buttonTag % 10;

                if (tempState[row + 1][col + 1] != boardState[row + 1][col + 1]) {
                    cellState(b);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void step(View v) {
        stepFunction();
    }

    private int[] getCellLocation(int tag) {

        int row = (int) tag / 10;
        int column = tag % 10;

        return new int[]{row, column};
    }

    private int getCellTag(int[] location) {
        return (location[0]*10) + location[1];
    }

    private void cellState(Button b) {
        cellState(b, false);
    }

    // boardState will toggle the color of the button and update the boardState array
    private void cellState(Button b, boolean firstRun) {


        int[] buttonRowColumn = getCellLocation((int) b.getTag());

        int presentState;

        if (firstRun) {
            presentState = 0;
            b.setBackgroundColor(deadCellColour);
        } else {
            presentState = boardState[buttonRowColumn[0] + 1][buttonRowColumn[1] + 1];
            if (presentState == 0) {
                presentState = 1;
                b.setBackgroundColor(aliveCellColour);
            } else {
                presentState = 0;
                b.setBackgroundColor(deadCellColour);
            }

        }

        boardState[buttonRowColumn[0] + 1][buttonRowColumn[1] + 1] = presentState;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        wrapAround = false;

        running = false;

        timerMillis = 500;

        aliveCellColour = Color.BLUE;
        deadCellColour = Color.RED;

        // Init

        handler = new Handler();

        runTimer(timerMillis);

        block = findViewById(R.id.block);

        // Setting up buttons
        stepButton = findViewById(R.id.stepButton);
        runButton = findViewById(R.id.runButton);
        clearButton = findViewById(R.id.clearButton);
        aboutButton = findViewById(R.id.aboutButton);
        settingsButton = findViewById(R.id.settingsButton);

        container = (ConstraintLayout) findViewById(R.id.container);

        switchBoard = (GridLayout) findViewById(R.id.switchBoard);

        boardState = new int[switchBoard.getRowCount() + 2][switchBoard.getColumnCount() + 2];

        for (int[] r : boardState) {
            Arrays.fill(r, 0);
        }

        final View.OnClickListener cellClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cellState((Button) view);
            }
        };

        container.post(new Runnable() {
            @Override
            public void run() {
//                boardWidth = container.getWidth();
//                boardHeight = switchBoard.getHeight();

                containerWidth = container.getWidth();


//                int padding = (int)(boardHeight - boardWidth) / 2;
//
//                switchBoard.setPadding(0, padding, 0, padding);
//
//                boardHeight = boardWidth;

                int cellPadding = 2;

                int extraPadding = 1;

                int cellWidth = containerWidth /switchBoard.getColumnCount() - 2*cellPadding - 2*extraPadding;
                int cellHeight = containerWidth /switchBoard.getRowCount() - 2*cellPadding - 2*extraPadding;

                for (int i = 0; i < 100; i++) {
                    Button button = new Button(getApplicationContext());

                    button.setTag(i);

                    // Cell is initially false
                    cellState(button, true);

                    button.setOnClickListener(cellClick);

                    GridLayout.LayoutParams lp = new GridLayout.LayoutParams();

                    lp.width = cellWidth;
                    lp.height = cellHeight;
                    lp.setMargins(cellPadding, cellPadding, cellPadding, cellPadding);

                    button.setLayoutParams(lp);

                    switchBoard.addView(button);
                }
            }
        });
    }
}
